# Infracost Bitbucket Pipeline

This project provides instructions for using Infracost in a Bitbucket Pipeline, it works with both Bitbucket Cloud and Data Center/Server. This enables you to see cloud cost estimates for Terraform in pull requests. 💰

![Example screenshot](.bitbucket/assets/pr-comment.png)

## Quick start

1. If you haven't done so already, [download Infracost](https://www.infracost.io/docs/#quick-start) and run `infracost auth login` to get a free API key.

2. Retrieve your Infracost API key by running `infracost configure get api_key`.

3. If you're using Bitbucket Cloud you can either use a App password (recommended) or a Repository access token.
    1. To generate a App password go to your Personal Settings > App passwords and generate a password that has read and write permissions for Repositories and Pull requests.
    2. To generate a Repository access token go to Repository Settings > Access tokens and generate a token that has read and write permissions for Repositories and Pull requests.

4. In Bitbucket, go to your Repository Settings, under Pipelines > Settings and "Enable Pipelines". In "Repository variables", add environment variables for:
    * `INFRACOST_API_KEY` - Your Infracost API key.
    * `BITBUCKET_TOKEN` - This is needed to post the pull request comment. For Bitbucket Cloud set this to the App password or Repository access token from step 3. For Bitbucket Server set this to your HTTP access token.

5. Create a new file at `bitbucket-pipelines.yml` in your repo with the following content.

```sh
# Infracost runs on pull requests and posts PR comments.
# If you use Infracost Cloud, Infracost also runs on main/master branch pushes so the dashboard is updated.
pipelines:
  pull-requests:
    '**':
      - step:
          name: Run Infracost on pull requests to check costs and policies
          # Always use the latest 0.10.x version to pick up bug fixes and new resources.
          image: infracost/infracost:ci-0.10
          script:
            # Clone the base branch of the pull request (e.g. main/master) into a temp directory.
            # You may want to use $BITBUCKET_GIT_SSH_ORIGIN if you're using a private repository
            - git clone $BITBUCKET_GIT_HTTP_ORIGIN --branch=$BITBUCKET_PR_DESTINATION_BRANCH --single-branch /tmp/base

            # If you use private modules, add an environment variable or secret
            # called GIT_SSH_KEY with your private key, so Infracost can access
            # private repositories (similar to how Terraform/Terragrunt does).
            # - |
            #   mkdir -p ~/.ssh
            #   eval `ssh-agent -s`
            #   echo "$GIT_SSH_KEY" | tr -d '\r' | ssh-add -
            #   # Update this to github.com, gitlab.com, bitbucket.org, ssh.dev.azure.com or your source control server's domain
            #   ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts

            # Generate an Infracost cost estimate baseline from the comparison branch, so that Infracost can compare the cost difference.
            - |
              infracost breakdown --path=/tmp/base \
                                  --format=json \
                                  --out-file=infracost-base.json

            # Generate an Infracost diff and save it to a JSON file.
            - |
              infracost diff --path=. \
                            --compare-to=infracost-base.json \
                            --format=json \
                            --out-file=infracost.json

            # Posts a comment to the PR using the 'update' behavior.
            # This creates a single comment and updates it. The "quietest" option.
            # The other valid behaviors are:
            #   delete-and-new - Delete previous comments and create a new one.
            #   new - Create a new cost estimate comment on every push.
            # For Bitbucket Cloud:
            #   If you're using a User token or App password:
            #     use --bitbucket-token=myusername:$BITBUCKET_TOKEN, where the token can be a User token or App password.
            #   If you're using a Repository access token:
            #     use --bitbucket-token=$BITBUCKET_TOKEN
            # For Bitbucket Server:
            #   use --bitbucket-token=$BITBUCKET_TOKEN with your HTTP access token.
            #   use --bitbucket-server-url=https://your-bitbucket-server.com to override the default https://bitbucket.org.
            # See https://www.infracost.io/docs/features/cli_commands/#comment-on-pull-requests for other options
            #   including --exclude-cli-output that posts only the summary table.
            - |
              infracost comment bitbucket --path=infracost.json \
                                          --repo=$BITBUCKET_WORKSPACE/$BITBUCKET_REPO_SLUG \
                                          --pull-request=$BITBUCKET_PR_ID \
                                          --bitbucket-token=alikhajeh1:$BITBUCKET_TOKEN \
                                          --behavior=update
  branches:
    '{main,master}':
      - step:
          name: Run Infracost on default branch and update Infracost Cloud
          image: infracost/infracost:ci-0.10
          script:
          - |
            infracost breakdown \
              --path=. \
              --format=json \
              --out-file=/tmp/infracost.json

            infracost upload --path=/tmp/infracost.json || echo "Always pass main branch runs even if there are policy failures"

      - step:
          name: Update PR status in Infracost Cloud
          image: infracost/infracost:ci-0.10
          script:
          - |
            PATTERN="pull request #([0-9]+)"
            if [[ "$(git show $BITBUCKET_COMMIT)" =~ $PATTERN ]]; then
              PR_ID=${BASH_REMATCH[1]}
              echo "Updating status of $PR_ID"

              curl \
                --request POST \
                --header "Content-Type: application/json" \
                --header "X-API-Key: ${INFRACOST_API_KEY}" \
                --data "{ \"query\": \"mutation { updatePullRequestStatus(url: \\\"${BITBUCKET_GIT_HTTP_ORIGIN}/pull-requests/${PR_ID}\\\", status: MERGED) }\" }" \
                "https://dashboard.api.infracost.io/graphql"
            else
              echo "Nothing to do as the commit message did not contain a merged PR ID."
            fi
```

6. Follow [these simple steps](https://www.infracost.io/docs/infracost_cloud/get_started/#4-send-a-pull-request) to test the integration. This is important as it ensures the CLI commands are running correctly in your workflow 👌

7. [Infracost Cloud](https://dashboard.infracost.io) is our SaaS product that builds on top of Infracost open source. It enables team leads, managers and FinOps practitioners to setup [tagging policies](https://www.infracost.io/docs/infracost_cloud/tagging_policies/), [guardrails](https://www.infracost.io/docs/infracost_cloud/guardrails/) and [best practices](https://www.infracost.io/docs/infracost_cloud/cost_policies/) to help guide the team. For example, you can check for required tag keys/values, or suggest switching AWS gp2 volumes to gp3 as they are more performant and cheaper.

    If you **do not** want to use [Infracost Cloud](https://dashboard.infracost.io), go to Org Settings and disable the dashboard. This causes the CLI not to send its JSON output to your dashboard; the JSON does not contain any cloud credentials or secrets, see the [FAQ](https://infracost.io/docs/faq/) for more information.

    <img src=".bitbucket/assets/infracost-cloud-dashboard.png" alt="Infracost Cloud enables you to check for best practices such as using latest generation instance types or block storage, as well as setup tagging policies and guardrails to help guide the team." width="80%" />

### Troubleshooting

#### HTTP 401 / 403 errors when posting comments

This is usually related to token issues, please use the following bash script to ensure that your token has the right access:

```sh
export BITBUCKET_REPO=myorg/myrepo

# For Bitbucket Server, set this to your server API URL
export BITBUCKET_SERVER=https://api.bitbucket.org

# For Bitbucket Server, use BITBUCKET_TOKEN=token, where the token is your HTTP access token (I don't think username is required)
export BITBUCKET_TOKEN=myusername:mytoken

# Set this to any commit just so you can test the curl call
export BITBUCKET_COMMIT=xxxxx

curl -i \
  -H 'Accept: application/json' \
  -u $BITBUCKET_TOKEN \
  $BITBUCKET_SERVER/2.0/repositories/$BITBUCKET_REPO/commit/$BITBUCKET_COMMIT

# If the response is HTTP 401 or 403, the token probably has issues
```

#### Error when cloning repository

If you see an error similar to `fatal: could not read Username for 'https://bitbucket.org': No such device or address` you can try updating the `- git clone $BITBUCKET_GIT_HTTP_ORIGIN...` line in your pipeline to use `- git clone $BITBUCKET_GIT_SSH_ORIGIN...` 

## Contributing

Issues and pull requests are welcome. Please create issues in [this repo](https://github.com/infracost/infracost) or [join our community Slack slack](https://www.infracost.io/community-chat), we are a friendly bunch and happy to help you get started :)

## License

[Apache License 2.0](https://choosealicense.com/licenses/apache-2.0/)
